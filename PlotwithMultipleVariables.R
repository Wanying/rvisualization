'persp plot
The function persp is the base graphics function for creating aireframe surface plots.
The persp function requires a list of x and y values covering the grid of vertical values 
which is specified as the z variable
The heights for the display are specfied as a table of values which we saved perviously as the object 
z duirng the caluclations when the local trend surface model was fitted to the data. 
'
# x <- seq(-10, 10, length= 30)
# y <- x
# f <- function(x, y) { r <- sqrt(x^2+y^2); 10 * sin(r)/r }
# z <- outer(x, y, f)
# z[is.na(z)] <- 1
# op <- par(bg = "white")
# #persp(x, y, z, theta = 30, phi = 30, expand = 0.5, col = "lightblue")
# persp(x, y, z, theta = 30, phi = 30, expand = 0.5, col = "lightblue",
#       ltheta = 120, shade = 0.75, ticktype = "detailed",
#       xlab = "X", ylab = "Y", zlab = "Sinc( r )") 


x <- y <- seq(-4*pi, 4*pi, len = 27)
r <- sqrt(outer(x^2, y^2, "+"))
image(z = z <- cos(r^2)*exp(-r/6), col  = gray((0:32)/32))