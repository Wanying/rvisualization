'fill with cross hash'
'barplot(VADeaths[1:2,],angle=c(45,135),density=20,col="grey",names=c("RM","RF","UM","UF"))
legend(0.4,38,c("55-59","50-54"),cex=1.5,angle=c(135,45),density=20,fill="grey")
'

'An Example of abline() and arrow'

# x<-runif(20,1,10) #uniform distribution, runif(n,min,max)
# y<-x+rnorm(20)
# plot(x,y,ann=FALSE, axes=FALSE, col="grey",pch=16)
# box(col="grey")
# lmfit<-lm(y~x)
# abline(lmfit)
# arrows(5,8,7,predict(lmfit, data.frame(x=7)),length=0.1)
# text(5,8,"Line of Best Fit")

'An example of rug'
'The rug() function produces a "rug" plot along of the axes, which consists of a series of
tick marks representing data locations'
y<-rnorm(50)
hist(y,main="",xlab="",ylab="",axes=FALSE, border="grey",col="light grey")
box(col="grey")
rug(y,ticksize=0.02)
